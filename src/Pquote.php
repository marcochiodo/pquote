<?php

namespace Pquote;

class Pquote
{
	
	public $Adapter;
	
	private $w_size; // seconds
	private $quote; // quote [*] is required
	private $namespace = ''; // if you want
	
	protected $Use;

	public function __construct( \Pquote\Adapter\AdapterInterface $Adapter , $options )
	{
		$this->Adapter = $Adapter;
		
		if (is_int($options['w_size']) || ctype_digit($options['w_size']))
			$this->w_size = (int) $options['w_size'];
		else
			$this->w_size = $this->parse_w_size($options['w_size']);
		
		if (! is_array($options['quote']) || empty($options['quote']['*']))
			throw new \Pquote\Exception\QuoteFormat();
		
		$this->quote = $options['quote'];
		
		if (isset($options['namespace']) && is_string($options['namespace']))
			$this->namespace = $options['namespace'];
	}
	
	
	public function can( $entity , $operation )
	{
		$value = $this->cleanLocal($entity, $operation)->getUse($entity, $operation);
		
		return ! ( is_int($value) && $value >= $this->getQuota($entity, $operation) );
	}
	
	public function inc( $entity , $operation )
	{
		$key = $this->getKey($entity, $operation);
		
		$incr = $this->Adapter->inc($key, $this->getTTL());
	
		$this->Use[$entity][$operation] = $incr;
		return $incr;
	}

	public function incIfCan( $entity , $operation )
	{
		$key = $this->getKey($entity, $operation);
		$quota = $this->getQuota($entity, $operation);
		
		$can = $this->Adapter->incIfCan($key, $quota, $this->getTTL());
		
		if( $can === false )
			return false;
		
		$this->Use[$entity][$operation] = $can;
		return true;		
	}
	
	public function getUse( $entity , $operation )
	{
		
		if( ! isset( $this->Use[$entity] , $this->Use[$entity][$operation] ) )
			$this->Use[$entity][$operation] = $this->Adapter->getUse( $this->getKey($entity, $operation) );
		
		return $this->Use[$entity][$operation];
	}
	
	public function reset( $entity , $operation = null )
	{
		if( $operation == null )
			$this->Adapter->reset( $this->getKeyFormat($entity, '*') );
		else
			$this->Adapter->reset( $this->getKey($entity, $operation) );
		
		return $this->cleanLocal($entity,$operation);
	}
	
	public function parse_w_size( $string )
	{
	
		if( ! preg_match('/^(\d{1,4})([smh]{1})$/', $string , $match) )
			throw new \Pquote\Exception\wSizeFormat();
	
		switch ( $match[2] ){
				
			case 's' :
				return (int) $match[1];
			case 'm' :
				return (int) $match[1] * 60;
			case 'h' :
				return (int) $match[1] * 3600;
		}
	}
	
	public function setRule( $entity , $operation , $quota )
	{
	
		$this->quote[$entity][$operation] = (int) $quota;
	
		return $this;
	}
	
	public function getQuota( $entity , $operation )
	{
	
		if( isset($this->quote[$entity][$operation]) )
			return (int) $this->quote[$entity][$operation];
		else
			return (int) $this->quote['*'][$operation];
	}
	
	public function getKey( $entity , $operation )
	{
		return $this->namespace . $entity . $operation;
	}
	
	public function getKeyFormat( $entity , $operation )
	{
		return [
			'namespace' => $this->namespace,
			'entity' => $entity,
			'operation' => $operation
		];
	}
	
	protected function cleanLocal( $entity , $operation = null )
	{
	
		if( $operation == null ){
			if( isset( $this->Use[$entity] ) )
				unset( $this->Use[$entity] );
		} else {
			if( isset( $this->Use[$entity] , $this->Use[$entity][$operation] ) )
				unset( $this->Use[$entity][$operation] );
		}
	
		return $this;
	}
	
	public function getTTL(){
		return $this->w_size;
	}
}

?>