<?php

namespace Pquote\Adapter;

interface AdapterInterface {
	
	public function can( $key , $quota );
	
	public function inc( $key , $ttlIfNew );
	
	public function incIfCan( $key , $quota , $ttlIfNew );
	
	public function getUse( $key );
	
	public function reset( $query );
	
}