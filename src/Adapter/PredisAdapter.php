<?php

namespace Pquote\Adapter;

class PredisAdapter implements AdapterInterface {
	
	private $Predis;
	private $options = [];
	
	public function __construct( \Predis\Client $Predis , $options = [] )
	{
		$this->Predis = $Predis;
		$this->options = $options;
	}
	
	public function can( $key , $quota )
	{
	
		return $this->getUse($key) < $quota;
	}
	
	public function inc( $key , $ttlIfNew )
	{
		
		$this->setDatabase();
		
		$incr = (int) $this->Predis->incr($key);
		
		if( $incr === 1 )
			$this->Predis->expire($key, $ttlIfNew);
		
		return $incr;
	}
	
	public function incIfCan( $key , $quota , $ttlIfNew )
	{

		$this->setDatabase();
		
		while(true){
	
			$this->Predis->watch( $key );
		
			$value = $this->Predis->get($key);
		
			if( ! $this->can($key , $quota) ){
				$this->Predis->unwatch();
				return false;
			}
			
			try {
				$this->Predis->multi();
				$this->Predis->incr($key);
				$Result = $this->Predis->exec();

				$incr = (int) $Result[0];
				if( $incr == 1 )
					$this->Predis->expire( $key , $ttlIfNew );
				
				return $incr;
			} catch (\Predis\Transaction\AbortedMultiExecException $e) {
				continue;
			}
		}		
	}
	
	public function getUse( $key )
	{
		
		$this->setDatabase();
		
		return (int) $this->Predis->get( $key );
	}
	
	public function reset( $query )
	{
		
		$this->setDatabase();
		
		if( is_array($query) ){
			
			$pattern = '';
			
			foreach( $query as $key => $item )
				if( $key == 'operation' )
					$pattern.='*';
				else
					$pattern.=$item;
				
			$keys = $this->Predis->keys($pattern);
		} else
			$keys = [ $query ];
		
		if( !empty($keys) )
			$this->Predis->del($keys);
		
		return $this;
	}
	
	private function setDatabase(){
		
		if( isset($this->options['database']) )
			$this->Predis->select( $this->options['database'] );
		
	}



}